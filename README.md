# pyml-plot

Thin matplotlib wrappers using `pyml`. See usage examples in `lib_test/plot_test.ml`.

# Running tests/examples:
- code in `./lib_test`
- if you have matplotlib installed, `dune runtest`
- if you don't, `bash virtualenv.sh` to install it locally then
  `dune exec lib_test/plot_test.exe --root=. -- venv/bin/python3`
